<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>The Coq Workshop 2024</title>

    <link rel="stylesheet" href="style.css">
	</head>
	<body>
    <header>
      <h1>The Coq Workshop 2024</h1>
      <p>
        Affiliated with <a href="https://www.viam.science.tsu.ge/itp2024/">ITP 2024</a>
      </p>
      <p>Tbilisi, Georgia</p>
      <p><strong>14th September, 2024</strong></p>
      <a href="/">(Past editions)</a>
    </header>

    <main>

      <div class="medium">
        <p>
          The Coq Workshop 2024 is the 15th instalment of the <a href="/">Coq
          Workshop series</a>. The workshop brings together developers,
          contributors, and users of the <a href="https://coq.inria.fr">Coq
          proof assistant</a>.

          The Coq Workshop focuses on strengthening
          the <a href="https://coq.inria.fr/community">Coq community</a> and
          providing a forum for discussing practical issues, including the
          future of the Coq software and its associated ecosystem of libraries
          and tools. Thus, rather than serving as a venue for traditional
          research papers, the workshop is organised around informal
          presentations and discussions.
        </p>
      </div>

      <div class="small">
        <h2>Important dates</h2>

        <div class="date">
          <p class="important"><s>29 May</s> <strong>7 June</strong>, 2024 (AoE)</p>
          <p>Submission deadline (⚠️ extended)</p>
        </div>

        <div class="date">
          <p class="important">3 July, 2024</p>
          <p>Author notification</p>
        </div>
      </div>

      <div>
        <h2>Attending</h2>

        <p>
          As the workshop is affiliated with ITP, attendees are required to
          register for the workshop through
          <a href="https://www.viam.science.tsu.ge/itp2024/registration">ITP registration</a>.
          The workshop (as well as the conference) can be attended either
          <strong>online</strong> or <strong>in person</strong>.
          Please consult the
          <a href="https://www.viam.science.tsu.ge/itp2024/registration">ITP registration page</a>
          for pricing information.
        </p>

        <p>
          One of the sessions will be affiliated with
          <a href="https://europroofnet.github.io/wg4-tbilisi24/">EuroProofNet</a>.
          As such participants of the Coq Workshop are also entitled to funding
          from EuroProofNet.
        </p>
        <p>
          See the
          <a href="https://europroofnet.github.io/wg4-tbilisi24/">
            EuroProofNet workshop website
          </a>
          for more details.
        </p>
      </div>

      <div id="program">
        <h2>Program</h2>

        <p>
          The following contributed talks were accepted for a presentation at
          the workshop.
        </p>

        <p>
          Times are local to Tbilisi.
        </p>

        <p>
          The talks will happen in <strong>room 319</strong>, although the
          invited talk will also be streamed in room 115 as part of the Isabelle
          workshop.
        </p>

        <table>
          <tbody>
            <tr>
              <th colspan="2" scope="rowgroup">Session 1</th>
            </tr>
            <tr>
              <td>9:30—9:40</td>
              <td>
                <div class="paper">
                  <h4>Welcome to the Coq Workshop</h4>
                  <p>Introduction from the organisers</p>
                </div>
              </td>
            </tr>
            <tr>
              <td>9:40—10:00</td>
              <td>
                <div class="paper">
                  <h4>On the Potential of Coq as the Platform of Choice for Hardware Design</h4>
                  <p><span class="speaker">Sebastian Ertel</span>, Max Kurze and Michael Raitza (online)</p>
                  <div class="files">
                    <a href="files/EA4.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL4.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      We present our experiences so far in bringing Coq-based hardware development to hardware engineers. More specifically, we chose Koika to re-implement the trusted communication unit (TCU), an essential kernel component in a micro-kernel-based operating system. We report on successes but also obstacles and derive future research directions.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>10:00—10:30</td>
              <td>
                <div class="paper">
                  <h4>A Development Process for Coq Projects Permitting Invalid Proofs</h4>
                  <p><span class="speaker">Hendrik Tews</span> (online)</p>
                  <div class="files">
                    <a href="files/EA1.pdf">🗎 Extended abstract</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      The talk presents a development process for Coq projects that permits invalid proofs in the main development line while still keeping control over the project. I review the existing tool support and present new Proof General features that enable such a process. The described process has recently been introduced at Kernkonzept.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
          </tbody>
          <tbody class="break">
            <tr>
              <td>10:30—11:00</td>
              <td>Coffee break</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th colspan="2" scope="rowgroup">
                Session 2
                <span class="small">
                  common with the <a href="https://europroofnet.github.io/wg4-tbilisi24/">EuroProofNet workshop</a>
                </span>
              </th>
            </tr>
            <tr>
              <td>11:00—11:30</td>
              <td>
                <div class="paper">
                  <h4>CoqPilot, a plugin for LLM-based generation of proofs</h4>
                  <p><span class="speaker">Andrei Kozyrev</span>, Gleb Solovev, Nikita Khramov and Anton Podkopaev (in person)</p>
                  <div class="files">
                    <a href="files/EA2.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL2.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      We present CoqPilot, a VS Code extension designed to help automate writing of Coq proofs. The plugin collects the parts of proofs marked with the <code>admit</code> tactic in a Coq file, i.e., proof holes, and combines LLMs along with non-machine-learning methods to generate proof candidates for the holes. Then, CoqPilot checks if each proof candidate solves the given subgoal and, if successful, replaces the hole with it. The focus of CoqPilot is twofold. Firstly, we want to allow users to seamlessly combine multiple Coq generation approaches and provide a zero-setup experience for our tool. Secondly, we want to deliver a platform for LLM-based experiments on Coq proof generation. We developed a benchmarking system for Coq generation methods, available in the plugin, and conducted an experiment using it, showcasing the framework's possibilities.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>11:30—11:50</td>
              <td>
                <div class="paper">
                  <h4>Blueprints for formalisation in Coq</h4>
                  <p><span class="speaker">Peter Lefanu Lumsdaine</span> (in person)</p>
                  <div class="files">
                    <a href="files/EA9.pdf">🗎 Extended abstract</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      I will discuss the use and usefulness of “blueprints” in formalisation, and the design practices they promote; and I will present a port for Coq of the <code>leanblueprint</code> tool
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>11:50—12:20</td>
              <td>
                <div class="paper">
                  <h4>Coq Platform docs: A Compilation of Short Interactive Tutorials and How-To Guides for Coq</h4>
                  <p><span class="speaker">Thomas Lamiaux</span>, Pierre Rousselin and Théo Zimmermann (online)</p>
                  <div class="files">
                    <a href="files/EA3.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL3.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      Dear Coq users and developers,
                      We would like to shed some light on our new project "Coq Platform Doc".
                      Our goal is to provide an online compilation of short, practical and interactive
                      tutorials and how-to guides about Coq and the Coq Platform. We wish this content
                      to be part of the upcoming Rocq website. A detailed description of the project can
                      be found in our Coq Enhancement Proposal (CEP) here:
                      <a href="https://github.com/coq/ceps/pull/91">https://github.com/coq/ceps/pull/91</a>.
                    </p>

                    <p>
                      The first few tutorials are available as well as a (very preliminary) online
                      interface here: <a href="https://www.theozimmermann.net/platform-docs/">https://www.theozimmermann.net/platform-docs/</a>.
                      At this moment, it contains:
                    </p>
                    <ul>
                      <li>a Search tutorial</li>
                      <li>a Basic library file and module management tutorial</li>
                      <li>3 tutorials about the Equation plugin</li>
                    </ul>
                    <p>
                      More will follow, especially with your help!
                    </p>

                    <p>
                      There is a dedicated Zulip channel to discuss and participate:
                      <a href="https://coq.zulipchat.com/#narrow/stream/437203-Coq-Platform-docs">https://coq.zulipchat.com/#narrow/stream/437203-Coq-Platform-docs</a>.
                      The preliminary git repository is <a href="https://github.com/Zimmi48/platform-docs">https://github.com/Zimmi48/platform-docs</a>.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
          </tbody>
          <tbody class="break">
            <tr>
              <td>12:30—14:00</td>
              <td>Lunch</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th colspan="2" scope="rowgroup">
                Session 3
                <span class="small">
                  invited talk shared with the
                  <a href="https://europroofnet.github.io/wg4-tbilisi24/">
                    EuroProofNet
                  </a>
                  and
                  <a href="https://sketis.net/isabelle/isabelle-workshop-2024">
                    Isabelle
                  </a>
                  workshops
                </span>
              </th>
            </tr>
            <tr>
              <td>14:00—15:00</td>
              <td>
                <div class="paper">
                  <h4>Invited talk: From 100 to 1000+ theorems</h4>
                  <p><span class="speaker">Freek Wiedijk</span> (online)</p>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      We have started a collaborative project to track which
                      of a list of around 1200 theorems have been formalized
                      in the major libraries of the main proof assistants for
                      mathematics: Isabelle, HOL Light, Coq, Lean, Metamath
                      and Mizar.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>15:00—15:30</td>
              <td>
                <div class="paper">
                  <h4>Lessons from Formalizing (Higher) Category Theory</h4>
                  <p><span class="speaker">Benedikt Ahrens</span> and Niels van der Weide (online)</p>
                  <div class="files">
                    <a href="files/EA6.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL6.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      UniMath is a library of univalent mathematics written in Coq. In the past few years,
                      it has seen growth mostly in the area of category theory and its applications to programming language semantics.
                      We give an overview of the UniMath library, some of the challenges that we are facing in developing and maintaining it, and our solutions to these challenges.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
          </tbody>
          <tbody class="break">
            <tr>
              <td>15:30—16:00</td>
              <td>Coffee break</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th colspan="2" scope="rowgroup">
                Session 4
              </th>
            </tr>
            <tr>
              <td>16:00—16:25</td>
              <td>
                <div class="paper">
                  <h4>Towards Formalising the Guard Condition of Coq</h4>
                  <p><span class="speaker">Yee Jian Tan</span> and Yannick Forster (online)</p>
                  <div class="files">
                    <a href="files/EA8.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL8.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      Coq's consistency — and the consistency of constructive type theories in general — crucially depends on all recursive functions being terminating. Technically, in Coq, this is ensured by a so-called guard checker, which is part of Coq's kernel and ensures that fixpoints defined on inductive types are structurally recursive. Coq's guard checker has been first implemented in the 1990s by Christine Paulin-Mohring, but was subsequently extended, adapted, and fixed by several contributors. As a result, there is no exact, abstract specification of it, meaning for instance that formal proofs about it are out of reach. We propose a talk on our ongoing work-in-progress project to synthesise a specification of Coq's guard checker as a guard condition predicate defined in Coq itself on top of the MetaCoq project. We hope that our project will benefit the users of Coq by providing an accurate and transparent description, as well as lay the foundations for future improvements or even mechanised consistency proofs of Coq.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>16:25—16:45</td>
              <td>
                <div class="paper">
                  <h4>Turning the Coq Proof Assistant into a Pocket Calculator</h4>
                  <p><span class="speaker">Guillaume Melquiond</span> (online)</p>
                  <div class="files">
                    <a href="files/EA5.pdf">🗎 Extended abstract</a>
                    &emsp;
                    <a href="files/SL5.pdf">📽️ Slides</a>
                  </div>
                  <details>
                    <summary>Abstract</summary>
                    <p>
                      User interfaces for the Coq proof assistant focus on the ability to write and verify proofs, and mostly ignore Coq's ability to compute. This talk shows how the tactic-in-term feature and some adhoc vernacular commands can provide a user experience closer to the one found in computer algebra systems. This work has been implemented in the CoqInterval library.
                    </p>
                  </details>
                </div>
              </td>
            </tr>
            <tr>
              <td>16:45—18:00</td>
              <td>
                <div class="paper">
                  <h4>Discussion with Coq developers</h4>
                  <p>Coq developers (online)</p>
                  <div class="files">
                    <a href="files/Coq-Development-Team-Session-Coq-Workshop-2024.pdf">📽️ Slides</a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div>
        <h2>Submission instructions</h2>

        <p class="breathe">
          Submissions should take the form of a
          <span class="important">two-page PDF</span>
          (excluding bibliography) and must be performed on
          <a href="https://easychair.org/conferences/?conf=coqws2024">Easychair</a>.
        </p>

        <p class="breathe">
          You have the freedom to produce the PDF by whatever means but keep in
          mind that it should remain legible for the people of the PC and for
          attendees of the conference so please avoid two pages of text with
          absolutely no margin.
        </p>

        <p class="breathe">
          We use a single-blind review process, meaning that reviewers have
          access to the identity and affiliations of the authors.
          As such, the submitted PDF should include name and affiliations in
          addition to the title and abstract.
        </p>

        <p class="breathe bold">
          Remote presentation is possible!
        </p>

        <a class="button" href="https://easychair.org/conferences/?conf=coqws2024">Submit to Coq Workshop 2024</a>
      </div>

      <div class="small">
        <h2>Organisers and contact</h2>

        <a class="nav" href="https://pit-claudel.fr/clement/">Clément Pit-Claudel</a>
        <a class="nav" href="https://theowinterhalter.github.io/">Théo Winterhalter</a>

        <a href="mailto:coqws2024@easychair.org">coqws2024@easychair.org</a>
      </div>

      <div class="medium">
        <h2>Program committee</h2>

        <ul>
          <li><a href="https://benediktahrens.gitlab.io/">Benedikt Ahrens</a> (TU Delft)</li>
          <li>Mireia González (Formal Vindications)</li>
          <li><a href="https://mir-ikbch.github.io/">Mirai Ikebuchi</a> (Kyoto University)</li>
          <li><a href="https://people.rennes.inria.fr/Assia.Mahboubi/">Assia Mahboubi</a> (Inria Rennes)</li>
          <li><a href="https://pit-claudel.fr/clement/">Clément Pit-Claudel</a> (EPFL) [chair]</li>
          <li><a href="https://swarnpriya.github.io/">Swarn Priya</a> (Virginia Tech)</li>
          <li><a href="https://people.mpi-sws.org/~msammler/">Michael Sammler</a> (ETH Zürich)</li>
          <li><a href="https://www-sop.inria.fr/members/Enrico.Tassi/">Enrico Tassi</a> (Inria Sophia)</li>
          <li><a href="https://theowinterhalter.github.io/">Théo Winterhalter</a> (Inria Saclay) [chair]</li>
          <li><a href="https://euisuny.github.io/">Irene Yoon</a> (Inria Paris)</li>
          <li><a href="https://perso.ens-lyon.fr/yannick.zakowski/">Yannick Zakowski</a> (Inria Lyon)</li>
        </ul>
      </div>

    </main>

  </body>
</html>
